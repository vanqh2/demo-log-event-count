package com.example.demologeventcount;

import com.example.demologeventcount.application.LogInfoConsumer;
import com.example.demologeventcount.application.Producer;
import com.example.demologeventcount.application.process.batchProcess.BatchConsumer;
import com.example.demologeventcount.application.process.batchProcess.BatchReading;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@Slf4j

public class DemoLogEventCountApplication implements CommandLineRunner {

//    @Autowired
//    private LogInfoConsumer logInfoConsumer;

    @Autowired
    private BatchConsumer batchConsumer;

//    @Autowired
//    private Producer producer;

    public static void main(String[] args) {
        SpringApplication.run(DemoLogEventCountApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
//        producer.sendMessage();

        Thread thread = new Thread(batchConsumer);
        thread.start();
//        Thread thread1 = new Thread(logInfoConsumer);
//        thread1.start();


    }
}
