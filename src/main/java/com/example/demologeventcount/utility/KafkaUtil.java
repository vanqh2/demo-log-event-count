package com.example.demologeventcount.utility;

import com.example.demologeventcount.application.Model.Message;
import com.example.demologeventcount.application.customSerde.JsonDeserializer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import java.util.Collections;
import java.util.Properties;

public class KafkaUtil {


    public static Consumer<String, Message> createConsumer(String group_id,String server ,String topic) {

        Properties properties = new Properties();
//        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBootstrapServer);
        //LOCAL
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,server);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG,group_id);
        properties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG,100);
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
        properties.put(ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG,5000);
        properties.put(JsonDeserializer.VALUE_CLASS_NAME_CONFIG,Message.class );

        Consumer<String,Message> consumer = new KafkaConsumer<String, Message>(properties);
        consumer.subscribe(Collections.singleton(topic));

        return consumer;

    }
}
