package com.example.demologeventcount.utility;

import org.springframework.beans.factory.annotation.Value;

import java.util.Collections;
import java.util.Set;

public class Setting {
    @Value("${spring.kafka.bootstrap-server}")
    public static Set<String> KAFKA_PUBLIC_BROKERS;

    @Value("${spring.kafka.topic}")
    public static String KAFKA_HISTORY_LOG_TOPIC;

    public static final String HADOOP_PATH = "hdfs://localhost:9000/";
}
