package com.example.demologeventcount.application.mvc.resources;

import com.example.demologeventcount.application.mvc.entity.LogInfo;
import com.example.demologeventcount.application.mvc.repository.LogInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component("messageResource")
@Configuration
public class MessageResource {
    @Autowired
    private LogInfoRepository repository;

    public LogInfo findLogInfo(Long order_id) {
        Optional<LogInfo> logInfo = repository.findById(order_id);
        if( ! logInfo.isPresent()) return null;
        return logInfo.get();
    }

    public void save(LogInfo logInfo) {
        repository.save(logInfo);
    }

    public int updateLogInfo(LogInfo logInfo) {
        return repository.updateLogInfo(logInfo.getTotal(), logInfo.getSucceed(),
                logInfo.getStart_time(),logInfo.getEnd_time(), logInfo.getOrder_id());
    }
    public int lockUpdating(Long order_id) {
        return repository.lockUpdating(order_id);
    }

    public String getFinalResult(Long order_id) {
        return repository.findById(order_id).get().getFinal_result();
//        return repository.getFinalResult(order_id);
    }

    public void updatePerDay( int total, int succeed, long start_time, long end_time, String finalResult, Long order_id) {
        repository.updatePerDay(total, succeed, start_time,end_time,finalResult,order_id);
    }

    public void updateFinalResult(String final_result, Long order_id){
        repository.updateFinalResult(final_result,order_id);
    }

}
