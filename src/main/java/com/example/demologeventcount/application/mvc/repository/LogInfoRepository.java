package com.example.demologeventcount.application.mvc.repository;

import com.example.demologeventcount.application.mvc.entity.LogInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface LogInfoRepository extends JpaRepository<LogInfo, Long> {


    @Modifying
    @Query("update LogInfo u set u.updating = 1 where u.order_id = ?1")
    int lockUpdating(long order_id);

    @Modifying
    @Query("update LogInfo u set u.total = ?1, u.succeed = ?2, u.start_time = ?3, u.end_time = ?4,u.get_new_data = 1, u.updating = 0 where u.order_id = ?5")
    int updateLogInfo(int total, int succeed, long start_time, long end_time, Long order_id);

    @Modifying
    @Query("select final_result from LogInfo where order_id = ?1 and get_new_data = 1")
    String getFinalResult(Long order_id);

    @Modifying
    @Query("update LogInfo u set u.final_result = ?1 where u.order_id = ?2")
    int updateFinalResult(String final_result, Long order_id);


    @Modifying
    @Query("update LogInfo u set u.total = ?1, u.succeed = ?2, u.start_time = ?3, u.end_time = ?4,u.final_result = ?5, u.get_new_data = 0, u.updating = 0 where u.order_id = ?6")
    int updatePerDay(int total, int succeed, long start_time, long end_time, String finalResult, Long order_id);

}
