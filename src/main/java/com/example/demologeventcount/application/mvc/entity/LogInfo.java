package com.example.demologeventcount.application.mvc.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "logInfo")
@Data
@NoArgsConstructor
@AllArgsConstructor

public class  LogInfo{

    @Id
    private Long order_id;

    @Column
    private int total;

    @Column
    private int succeed;

    @Column
    private long start_time;

    @Column
    private long end_time;

    @Column
    private int updating;

    @Column
    private String final_result;

    @Column
    private int get_new_data;

    public LogInfo(Long order_id, int total, int succeed, long start_time, long end_time) {
        this.order_id = order_id;
        this.total = total;
        this.succeed = succeed;
        this.start_time = start_time;
        this.end_time = end_time;
        this.get_new_data = 1;
    }
}
