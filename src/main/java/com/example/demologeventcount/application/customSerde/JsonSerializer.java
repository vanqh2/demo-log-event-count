package com.example.demologeventcount.application.customSerde;

import org.apache.commons.lang.SerializationException;
import org.apache.kafka.common.serialization.Serializer;
import com.fasterxml.jackson.databind.ObjectMapper;
public class JsonSerializer<T> implements Serializer<T> {
    private final com.fasterxml.jackson.databind.ObjectMapper objectMapper = new ObjectMapper();


    @Override
    public byte[] serialize(String topic, T data) {
        if (data == null) {
            return null;
        }
        try {
            return objectMapper.writeValueAsBytes(data);
        } catch (Exception e) {
            throw new SerializationException("Error serializing JSON message", e);
        }
    }
}
