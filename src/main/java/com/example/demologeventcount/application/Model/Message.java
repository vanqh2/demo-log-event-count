package com.example.demologeventcount.application.Model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Message {
    private Long order_id;
    private int total;
    private int succeed;
    private long start_time;
    private long end_time;
    public void randMessage() {
        this.order_id = 26072200L + randInt(1,30);
        this.total = randInt(50,150);
        this.succeed = this.total - randInt(0,15);
        this.end_time = System.currentTimeMillis() - randInt(0,60)*1000;
        this.start_time = this.end_time - randInt(0,60)*1000;
    }

    private int randInt(int start, int end) {
        double rand = Math.random();
        int randInt = (int) (rand * (end - start) + start);
        return randInt;
    }

    public Message createNewObject(long order_id) {
        this.order_id = order_id;
        this.total = 0;
        this.succeed = 0;
        this.start_time = -1l;
        this.end_time = -1l;
        return this;
    }

    public String toString() {
        return "{" +
                " \"order_id\": " + this.order_id + ", " +
                "\"total\": " + this.total + ", " +
                "\"succeed\": " + this.succeed + ", " +
                "\"start_time\": " + this.start_time + ", " +
                "\"end_time\": " + this.end_time +
                "}";
    }
}

   /* public void updateValue(int total, int succeed, long start_time, long end_time) {
        this.total += total;
        this.succeed += succeed;
        if(this.start_time > start_time) this.start_time = start_time;
        if(this.end_time < end_time) this.end_time = end_time;
    }*/
