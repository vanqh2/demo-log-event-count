package com.example.demologeventcount.application.process;

import com.example.demologeventcount.application.Model.Message;
import com.example.demologeventcount.application.mvc.entity.LogInfo;
import com.example.demologeventcount.application.mvc.resources.MessageResource;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Configuration
@NoArgsConstructor

public class DataProccess {


    @Qualifier("messageResource")
    @Autowired
    private MessageResource messageResource;

    public void  updateData(List<Message> messageList) throws InterruptedException {
        Map<Long,Message> newList = mapReduceWithRecords(messageList);
        for(Map.Entry<Long,Message> entry : newList.entrySet()) {
            Message message = entry.getValue();
            LogInfo logInfo =  messageResource.findLogInfo(message.getOrder_id());
            LogInfo newLog = new LogInfo(message.getOrder_id(),message.getTotal(),
                    message.getSucceed(),message.getStart_time(),message.getEnd_time());
            if(logInfo == null) {
                Message newFinal = new Message().createNewObject(message.getOrder_id());
                newLog.setFinal_result(newFinal.toString());
                messageResource.save(newLog);
            }
            else  {
                int update = logInfo.getUpdating();
                while (update == 1){
                    Thread.sleep(500);
                    update = messageResource.findLogInfo(message.getOrder_id()).getUpdating();
                }
                messageResource.lockUpdating(logInfo.getOrder_id());
                logInfo = updateLogInfoValue(message,logInfo);
                messageResource.updateLogInfo(logInfo);
            }
        }

    }

    public void updatePerDay(List<Message> messageList) throws IOException {
        Map<Long,Message> newList = mapReduceWithRecords(messageList);
        for(Map.Entry<Long,Message> entry : newList.entrySet()) {
            Message message = entry.getValue();
            String logInfoString =  messageResource.getFinalResult(message.getOrder_id());
            Message tmpResult = new ObjectMapper().readValue(logInfoString, Message.class);
            tmpResult = updateValue(tmpResult, message);
            messageResource.updateFinalResult(tmpResult.toString(),tmpResult.getOrder_id());
            messageResource.updatePerDay(tmpResult.getTotal(), tmpResult.getSucceed(), tmpResult.getStart_time(),
                    tmpResult.getEnd_time(), tmpResult.toString(), tmpResult.getOrder_id());
        }
    }

    public Message updateValue(Message containMessage, Message message) {
        containMessage.setTotal(containMessage.getTotal() + message.getTotal());
        containMessage.setSucceed(containMessage.getSucceed() + message.getSucceed());
        if(containMessage.getStart_time() > message.getStart_time() || containMessage.getStart_time() == -1 ) containMessage.setStart_time(message.getStart_time());
        if(containMessage.getEnd_time() < message.getEnd_time() || containMessage.getEnd_time() == -1 ) containMessage.setEnd_time(message.getEnd_time());

        return containMessage;
    }

    private Map<Long, Message> mapReduceWithRecords(List<Message> messageList) {
        HashMap<Long,Message> map = new HashMap<>();

        for(Message message : messageList) {
            if(! map.containsKey(message.getOrder_id())) {
                map.put(message.getOrder_id(),message);
            }
            else {
                Message oldValue = map.get(message.getOrder_id());
                oldValue.setTotal(oldValue.getTotal() + message.getTotal());
                oldValue.setSucceed(oldValue.getSucceed() + message.getSucceed());
                if(oldValue.getStart_time() > message.getStart_time() ) oldValue.setStart_time(message.getStart_time());
                if(oldValue.getEnd_time() < message.getEnd_time() ) oldValue.setEnd_time(message.getEnd_time());
                map.put(oldValue.getOrder_id(),oldValue);
            }
        }
        return map;
    }

    private LogInfo updateLogInfoValue(Message message, LogInfo logInfo) {
        logInfo.setTotal(logInfo.getTotal() + message.getTotal());
        logInfo.setSucceed(logInfo.getSucceed() + message.getSucceed());
        if(logInfo.getStart_time() > message.getStart_time() ) logInfo.setStart_time(message.getStart_time());
        if(logInfo.getEnd_time() < message.getEnd_time() ) logInfo.setEnd_time(message.getEnd_time());

        return logInfo;
    }

//    public void updateRealtimeSpeed() {
//        List<LogInfo> logInfoList = messageResource.findAllOrderUpdate();
////        List<Message> messageList = new ArrayList<>();
//        logInfoList.forEach(logInfo -> {
//
//            try {
//                Message tmpResult = new ObjectMapper().readValue(logInfo.getFinal_result(), Message.class);
//                LogInfo data = new LogInfo(tmpResult.getOrder_id(), tmpResult.getTotal(),
//                        tmpResult.getSucceed(), tmpResult.getStart_time(), tmpResult.getEnd_time());
//                messageResource.updateLogInfo(data);
//            } catch (IOException e) {
//                throw new RuntimeException(e);
//            }
//        });
//    }

}
