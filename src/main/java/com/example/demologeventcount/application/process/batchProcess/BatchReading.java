package com.example.demologeventcount.application.process.batchProcess;

import com.example.demologeventcount.application.Model.Message;
import com.example.demologeventcount.application.process.DataProccess;
import com.example.demologeventcount.utility.Setting;
import org.apache.avro.generic.GenericData;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.avro.AvroParquetReader;
import org.apache.parquet.hadoop.ParquetReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
@Component
@EnableScheduling
public class BatchReading {

    Logger logger = LoggerFactory.getLogger(BatchReading.class);
    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    private Configuration configuration;

    @Autowired
    public DataProccess dataProccess;

    public BatchReading() throws FileNotFoundException {
//        Configuration configuration = new Configuration();
        this.configuration = new Configuration();
        File coreSiteFile = new File("config/core-site.xml");
        File hdfsSiteFile = new File("config/hdfs-site.xml");

        InputStream coreSite = new FileInputStream(coreSiteFile);
        this.configuration.addResource(coreSite);

        InputStream hdfsSite = new FileInputStream(hdfsSiteFile);
        this.configuration.addResource(hdfsSite);
    }

    @Scheduled(cron = "0 1 1 * * *", zone = "Asia/Ho_Chi_Minh")
    public void getData() {
        logger.info("INFO: FUNCTION GET DATA {}", "read data from hdfs");
        ParquetReader<GenericData.Record> reader = null;
        Path path =    new    Path(getFolder() + "");
        try {
            reader = AvroParquetReader
                    .<GenericData.Record>builder(path)
                    .withConf(this.configuration)
                    .build();
            GenericData.Record record;
            List<Message> messageList = new ArrayList<>();
            while ((record = reader.read()) != null) {
                Message message = getMessage(record);
                messageList.add(message);
                System.out.println(record);
            }
            dataProccess.updatePerDay(messageList);
        }catch(IOException e) {
            e.printStackTrace();
        }finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        logger.info("INFO: COMPLETE GET DATA: ","complete read from hdfs");
    }

    private Message getMessage(GenericData.Record record) {
        Message message = new Message();
        message.setOrder_id((Long) record.get("order_id"));
        message.setTotal((Integer) record.get("total"));
        message.setSucceed((Integer) record.get("succeed"));
        message.setStart_time((Long) record.get("start_time"));
        message.setEnd_time((Long) record.get("end_time"));
        return message;
    }

    public String getFolder() {
        return Setting.HADOOP_PATH + "demo_log_event_count" + getYesterday();

    }

    public String getYesterday() {
//        return formatter.format(new Date(System.currentTimeMillis()));
        return formatter.format(new Date(System.currentTimeMillis()-24*60*60*1000));
    }
}
