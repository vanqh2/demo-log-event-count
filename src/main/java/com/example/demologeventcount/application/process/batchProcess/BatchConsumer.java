package com.example.demologeventcount.application.process.batchProcess;

import com.example.demologeventcount.application.Model.Message;
import com.example.demologeventcount.utility.KafkaUtil;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
@Component
public class BatchConsumer implements Runnable {
    private boolean keepConsuming = true;

    @Value("${spring.kafka.bootstrap-servers}")
    private String server;
    @Value("${spring.kafka.topic}")
    private String topic;

    @Autowired
    private BatchSaving batchSaving;
    @Override
    public void run() {
        while (keepConsuming) {
            Consumer<String, Message> consumer = KafkaUtil.createConsumer("test-vanpt-batch",server, topic);
            while (keepConsuming) {
                ConsumerRecords<String, Message> consumerRecords = consumer.poll(Duration.ofMillis(5000));
                if (consumerRecords.count() > 0)
                {
                    List<Message> messageList = new ArrayList<>();
                    for (ConsumerRecord<String, Message> record : consumerRecords) {
                        Message newMessage = record.value();
                        messageList.add(newMessage);
                    }
                    try {
                        batchSaving.insertLog(messageList);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
                consumer.commitAsync();
            }
        }

    }
}
