package com.example.demologeventcount.application.process.batchProcess;

import com.example.demologeventcount.application.Model.Message;
import com.example.demologeventcount.utility.Setting;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.avro.AvroParquetWriter;
import org.apache.parquet.hadoop.ParquetWriter;
import org.apache.parquet.hadoop.metadata.CompressionCodecName;
import org.apache.parquet.hadoop.util.HadoopOutputFile;
import org.apache.parquet.io.OutputFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
@EnableScheduling
public class BatchSaving {
    private static final Logger logger = LoggerFactory.getLogger(BatchSaving.class);
    private FileSystem fs;

    private Path path;
    private Configuration configuration;
    private ParquetWriter<GenericData.Record> writer;
    private static Schema SCHEMA;
    private static String SCHEMA_LOCATION = "/ObjectToParquet.avsc";
    private static String EXTENTION = "parquet";
    private static long FILE_SIZE = 1024*2024*128;
    public void closeWriter(){
        try {
            this.writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    static {
        try (InputStream inStream = new ClassPathResource(SCHEMA_LOCATION).getInputStream()) {
            SCHEMA = new Schema.Parser().parse(IOUtils.toString(inStream, StandardCharsets.UTF_8));
        } catch (IOException e) {
            logger.error("Can't read SCHEMA file from {}", SCHEMA_LOCATION);
            throw new RuntimeException("Can't read SCHEMA file from" + SCHEMA_LOCATION, e);
        }
    }

    public BatchSaving() throws FileNotFoundException {
//        Configuration configuration = new Configuration();
        this.configuration = new Configuration();
        File coreSiteFile = new File("config/core-site.xml");
        File hdfsSiteFile = new File("config/hdfs-site.xml");

        InputStream coreSite = new FileInputStream(coreSiteFile);
        this.configuration.addResource(coreSite);

        InputStream hdfsSite = new FileInputStream(hdfsSiteFile);
        this.configuration.addResource(hdfsSite);
    }

    @Scheduled(cron = "0 0 1 * * *", zone = "Asia/Ho_Chi_Minh")
    public void BatchProcess() throws IOException {
        logger.info("BATCH SAVING: FUNCTION NEW FILE: ", "close writer to save data");
//        this.writer.close();
        nextFile();

    }

    @PreDestroy
    private void destroy() {
        try {
            this.writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public FileSystem getFileSystem() throws IOException {
        if(fs == null) {
            synchronized (BatchSaving.class) {
                fs = FileSystem.get(configuration);
            }
        }
        return fs;
    }

    protected void createTypeDir(Path path) throws IOException {
        FileSystem fileSystem = this.getFileSystem();
        if (!fileSystem.exists(path)) {
            fileSystem.mkdirs(path);
        }
    }

    private String getFolder(String day) {
        return Setting.HADOOP_PATH + "demo_log_event_count" + day;
    }

    private Path getNewFile(Path path) {
        String newFile = path.toString() + '/' + new Timestamp(System.currentTimeMillis()).getTime() + "." + EXTENTION;
        return new Path(newFile);
    }

    private Optional<Path> getPath() {
        String folderType = getFolder(formatter.format(new Date(System.currentTimeMillis())));
        try {
            Path path = new Path(folderType);
            createTypeDir(path);
            return Optional.of(getNewFile(path));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    private List<GenericData.Record> getRecord(List<Message> messages) {
        List<GenericData.Record> recordList = new ArrayList<>();
        messages.forEach(message ->{
            GenericData.Record record = new GenericData.Record(SCHEMA);
            record.put("order_id",message.getOrder_id());
            record.put("total",message.getTotal());
            record.put("succeed",message.getSucceed());
            record.put("start_time",message.getStart_time());
            record.put("end_time",message.getEnd_time());

            recordList.add(record);
        } );
        return recordList;
    }

    public void createWriter() throws IOException {
        Optional<Path> optionalPath = this.getPath();
        if(!optionalPath.isPresent()) return;

        path = optionalPath.get();
        OutputFile outputFile = HadoopOutputFile.fromPath(path, this.configuration);
//        System.out.println("get output file  :" + outputFile.getPath());
        this.writer = AvroParquetWriter.<GenericData.Record>builder(outputFile)
                .withSchema(SCHEMA)
                .withConf(this.configuration)
                .withCompressionCodec(CompressionCodecName.SNAPPY)
                .build();
    }

    public synchronized void insertLog(List<Message> messages) throws IOException {
        if(messages.isEmpty()) return;
        validate();
        writeToParquet(messages);


    }

    private void validate() throws IOException {
        if(this.writer == null){
            createWriter();
            return;
        }
        if(writer.getDataSize()  > FILE_SIZE - 100* 1024) nextFile();
    }

    public void nextFile() throws IOException {
        ParquetWriter<GenericData.Record> oldWriter = this.writer;
        Path oldPath = this.path;
        createWriter();
        close(oldWriter,oldPath);

    }

    private void close(ParquetWriter<GenericData.Record> oldWriter, Path oldPath) {
        boolean isSave = true;
        if(oldWriter !=  null) {
            try {

                isSave = oldWriter.getDataSize() > 1024;
                oldWriter.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    FileSystem fileSystem = this.getFileSystem();
                    logger.info("delete empty file: ",oldPath);
                    if(! isSave && fileSystem.exists(oldPath)) {
                        fileSystem.delete(oldPath,true);
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public void writeToParquet(List<Message> messages) throws IOException {
        List<GenericData.Record> recordList = getRecord(messages);
//        System.out.println("get path: " + this.path);
        for(GenericData.Record record : recordList) {
            writer.write(record);

        }
    }
}
