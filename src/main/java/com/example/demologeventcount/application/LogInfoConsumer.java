package com.example.demologeventcount.application;

import com.example.demologeventcount.application.Model.Message;
import com.example.demologeventcount.application.customSerde.JsonDeserializer;
import com.example.demologeventcount.application.mvc.entity.LogInfo;
import com.example.demologeventcount.application.process.DataProccess;
import com.example.demologeventcount.utility.KafkaUtil;
import org.apache.kafka.clients.consumer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.*;

@Component
public class LogInfoConsumer implements Runnable {

    Logger logger = LoggerFactory.getLogger(LogInfoConsumer.class);

    private boolean keepConsuming = true;

    @Value("${spring.kafka.bootstrap-servers}")
    private String server;
    @Value("${spring.kafka.topic}")
    private String topic;

    @Autowired
    DataProccess dataProccess;

    @Override
    public void run() {
        while (keepConsuming) {
            Consumer<String, Message> consumer = KafkaUtil.createConsumer("test-vanpt",server, topic);
            logger.info("kafka topic " + topic);
            while (keepConsuming) {
                ConsumerRecords<String, Message> consumerRecords = consumer.poll(Duration.ofMillis(5000));
                if (consumerRecords.count() == 0) continue;
                List<Message> messageList = new ArrayList<>();
                for (ConsumerRecord<String, Message> record : consumerRecords) {
                    Message newMessage = record.value();
                    messageList.add(newMessage);
                }
                try {
                    dataProccess.updateData(messageList);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                consumer.commitAsync();
            }
        }
    }

}
