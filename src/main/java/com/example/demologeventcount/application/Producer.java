package com.example.demologeventcount.application;

import com.example.demologeventcount.application.Model.Message;
import com.example.demologeventcount.application.customSerde.JsonSerializer;
import com.example.demologeventcount.utility.Setting;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Properties;
@Component
public class Producer {
    @Value("${spring.kafka.topic}")
    private String topic;

    public void sendMessage() {
        final Properties properties = new Properties();
//        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,Setting.KAFKA_PUBLIC_BROKERS);
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        KafkaProducer producer = new KafkaProducer<String,Message>(properties);

        System.out.println("kafka topic: " +topic );

        for(int i = 0; i< 500; i++) {
            Message message = new Message();
            message.randMessage();
            ProducerRecord<String, Message> record = new ProducerRecord<>(topic,message);
            producer.send(record);
        }
        producer.flush();
        producer.close();
    }
    
}
